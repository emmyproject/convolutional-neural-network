#**************************************************************************
#EM 23/09/2021
#Builds a very deep convolutional network, using Residual Networks (ResNets).
#Implement the basic building blocks of ResNets in a deep neural network
#using Keras
#Put together these building blocks to implement and train a state-of-the-art
#neural network for image classification
#Implement a skip connection in your network
#**************************************************************************

import textwrap
from module_functions import *
import tensorflow as tf
import numpy as np
import os
from glob import glob
import scipy.misc
from tensorflow.keras.applications.resnet_v2 import ResNet50V2
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.resnet_v2 \
    import preprocess_input, decode_predictions
from tensorflow.keras import layers
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers \
    import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization,\
    Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D
from tensorflow.keras.initializers \
    import random_uniform, glorot_uniform, constant, identity
from tensorflow.python.framework.ops import EagerTensor
from matplotlib.pyplot import imshow
#----------------------------------------------------------------------------

print(fg.B_BLUE+'\nBUILDING A RESIDUAL NETWORK (RESNET)')
print('=================================================\n'+style.ENDC)

print (fg.RED+textwrap.fill('NOTE: If you like more details about'
                     'the dataset and display a sample training image, '
                     'set "print_out = True" in function of '
                     'norm_reshape_image.')+'\n'+style.ENDC)

print ('Reasing the train and test data sets ...\n')
data=dataset('train_signs.h5','test_signs.h5')
X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes=\
    data.load_dataset()

X_train, Y_train, X_test, Y_test = \
    data.norm_reshape_image(X_train_orig, Y_train_orig, X_test_orig,\
                            Y_test_orig, \
                            'functional',print_out = False)


print ('Checking whether the model is already trained ...\n')


dir = os.getcwd()
if dir != '/' and glob( dir + '/resnet50.h5' ):
    print (fg.GREEN+textwrap.fill('resnet50.h5 file already exists. '
                                  'You may use the pre-trained model')+\
           style.ENDC+'\n')
else:
    print (fg.GREEN+textwrap.fill('resnet50.h5 file does not exist,'
                                  'you need to build the RESNET model')\
           +style.ENDC+'\n')
        
while True:
    print ('Select one of the below options: \n'
                         '(1) Build the RESNET model.\n'
                         '(2) Use the pre-trained model.'+'\n')

    
    try:
        ichoice=int(input('Please enter 1 or 2: '))
    except ValueError:
        ichoice=-1
    if ichoice>=1 and ichoice<=2:
        break
    print("Try again.  Enter an integer 1 or 2.\n")
print(' ')
    
if ichoice==1:
    print (textwrap.fill('building the model graph ...')+'\n')
    input("Press Enter to continue...")

    model = ResNet50(input_shape = (64, 64, 3), classes = 6)
    print(model.summary())

    #configure the learning process by compiling the model.
    model.compile(optimizer='adam', loss='categorical_crossentropy',\
                  metrics=['accuracy'])

    #The model is now ready to be trained.     
    #Train the model on the desired epochs and batch size
    print (style.BOLD+'Training the model ... \n'+style.ENDC)
    model.fit(X_train, Y_train, epochs = 10, batch_size = 32)
    
    #Save the trained model in the format of h5
    print (style.BOLD+'Saving the trained model ...\n'+style.ENDC)
    model.save('resnet50.h5')

    preds = model.evaluate(X_test, Y_test)
    
    print (style.BOLD+'\nEvaluating the trained model on test set gives: '\
       +style.ENDC)
    print ('Loss = ' + str(preds[0]))
    print ('Test Accuracy = ' + str(preds[1]))

    
if ichoice==2:
    pass


#You can load and run the trained model on the test set in the below
pre_trained_model = tf.keras.models.load_model('resnet50.h5')
preds = pre_trained_model.evaluate(X_test, Y_test)

print (style.BOLD+'\nEvaluating the pre-trained model on test set gives: '\
       +style.ENDC)
print ('Loss = ' + str(preds[0]))
print ('Test Accuracy = ' + str(preds[1]))

#Test on Your Own Image
print ('\nChecking the model on a random image ...\n')

img_path = 'images/my_image_1.jpeg'
img = image.load_img(img_path, target_size=(64, 64))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = x/255.0

print('Input image shape:', x.shape)
imshow(img)
plt.show()

prediction = pre_trained_model.predict(x)

print("Class prediction vector [p(0), p(1), p(2), p(3), p(4), p(5)] = ",\
      prediction)
print("Class:", np.argmax(prediction))

target_img = image.load_img('images/signs_data_kiank.png')
imshow(target_img)

plt.show()

