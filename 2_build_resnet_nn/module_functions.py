
import h5py
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.layers \
    import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization,\
    Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D
from tensorflow.keras.initializers \
    import random_uniform, glorot_uniform, constant, identity
from tensorflow.keras.models import Model


#-------------------------------------------------------------------------
#ANSI colour codes
class fg:
    BLACK   = '\033[30m'
    RED     = '\033[31m'
    GREEN   = '\033[32m'
    YELLOW  = '\033[33m'
    BLUE    = '\033[34m'
    B_BLUE  = '\033[1;34m'
    B_RED   = '\033[1;31m'
    B_GREEN = '\033[1;32m'
    CYAN    = '\033[36m'
    WHITE   = '\033[37m'
    RESET   = '\033[39m'

class bg:
    BLACK   = '\033[40m'
    RED     = '\033[41m'
    GREEN   = '\033[42m'
    YELLOW  = '\033[43m'
    BLUE    = '\033[44m'
    MAGENTA = '\033[45m'
    CYAN    = '\033[46m'
    WHITE   = '\033[47m'
    RESET   = '\033[49m'

class style:
    BOLD      = '\033[1m'
    DIM       = '\033[2m'
    NORMAL    = '\033[22m'
    ENDC      = '\033[0m'
    UNDERLINE = '\033[4m'
    ITALIC    = "\033[3m"
    BLINK     = "\033[5m"


#Class of loading, impleenting and plotting data

class dataset():

    def __init__(self,train_name,test_name):
        self.train_name=train_name
        self.test_name=test_name

    def load_dataset(self):
        train_dataset = h5py.File('datasets/'+self.train_name, "r")

        # your train set features
        train_set_x_orig = np.array(train_dataset["train_set_x"][:])

        # your train set labels
        train_set_y_orig = np.array(train_dataset["train_set_y"][:]) 

        test_dataset = h5py.File('datasets/'+self.test_name, "r")

        # your test set features
        test_set_x_orig = np.array(test_dataset["test_set_x"][:])

        # your test set labels
        test_set_y_orig = np.array(test_dataset["test_set_y"][:]) 

        # the list of classes
        classes = np.array(test_dataset["list_classes"][:]) 
    
        train_set_y_orig = train_set_y_orig.reshape((1, \
                                                     train_set_y_orig.shape[0]))
        test_set_y_orig = test_set_y_orig.reshape((1, \
                                                   test_set_y_orig.shape[0]))
    
        return train_set_x_orig, train_set_y_orig, test_set_x_orig,\
            test_set_y_orig, classes


    def display_sample(self,dataset):

        print ('Do you like to display a sample training image?')
        ans=str(input('(y/n): \n')).lower().strip()
        try:
            if ans[0] == 'y':
                print('\nPlease enter an image index\n')
                image_index=int(input('between 0 and '\
                                      +str(dataset.shape[0]-1)+'\n'))
                plt.imshow(dataset[image_index])
                plt.show()

            elif ans[0] == 'n':
                pass
            else:
                print ('\nTry again.')
                return self.display_sample(dataset)
        except ValueError:
            print('Please enter valid inputs')
            print('error')
            return self.display_sample()
            


    def norm_reshape_image(self, x_train, y_train, x_test, y_test, \
                           func,print_out = False):
        # Normalize image vectors
        x_train = x_train/255.
        x_test = x_test/255.

        # Reshape
        if func == 'seq':
            y_train = y_train.T
            y_test = y_test.T
        if func == 'functional':
            #6 type of signs there are in the dataset 
            y_train = np.eye(6)[y_train.reshape(-1)]
            y_test = np.eye(6)[y_test.reshape(-1)]

        if print_out == True:
            print ('------------------------------------------------------')
            print (style.BOLD+\
                   'Detail of ', self.train_name,'and',self.test_name,'sets:'\
                   +style.ENDC)
            print ("Number of training examples = " + str(x_train.shape[0]))
            print ("Number of test examples = " + str(x_test.shape[0]))
            print ("X_train shape: " + str(x_train.shape))
            print ("Y_train shape: " + str(y_train.shape))
            print ("X_test shape: " + str(x_test.shape))
            print ("Y_test shape: " + str(y_test.shape))
            print ('------------------------------------------------------')

            self.display_sample(x_train)

        return x_train, y_train, x_test, y_test
 
#--------------------------------------------------------------------------    

# GRADED FUNCTION: identity_block

def identity_block(X, f, filters, training=True, initializer=random_uniform):
    """
    Implementation of the identity block, which Skip connection "skips over" 
    3 layers.
    
    Arguments:
    X -- input tensor of shape (m, n_H_prev, n_W_prev, n_C_prev)
    f -- integer, specifying the shape of the middle CONV's window for 
         the main path

    filters -- python list of integers, defining the number of filters 
               in the CONV layers of the main path
    training -- True: Behave in training mode
                False: Behave in inference mode
    initializer -- to set up the initial weights of a layer. 
                   Equals to random uniform initializer
    
    Returns:
    X -- output of the identity block, tensor of shape (m, n_H, n_W, n_C)
    """
    
    # Retrieve Filters
    F1, F2, F3 = filters
    
    # Save the input value. This later will be added back to the main path. 
    X_shortcut = X
    
    # First component of main path
    X = Conv2D(filters = F1, kernel_size = 1, strides = (1,1), \
               padding = 'valid',\
               kernel_initializer = initializer(seed=0))(X)
    X = BatchNormalization(axis = 3)(X, training = training) # Default axis
    X = Activation('relu')(X)
    
    # Second component of main path (≈3 lines)
    X = Conv2D(filters = F2, kernel_size = (f,f), strides = (1,1),\
               padding = 'same',\
               kernel_initializer = initializer(seed=0))(X)
    X = BatchNormalization(axis = 3)(X, training = training) # Default axis
    X = Activation('relu')(X) 

    # Third component of main path (≈2 lines)
    X = Conv2D(filters = F3, kernel_size = 1, strides = (1,1),\
               padding = 'valid',\
               kernel_initializer = initializer(seed=0))(X)
    X = BatchNormalization(axis = 3)(X, training = training) # Default axis
    
    # Final step: Add shortcut value to main path, and pass it through
    # a RELU activation
    X = Add()([X,X_shortcut])
    X = Activation('relu')(X)

    return X


# GRADED FUNCTION: convolutional_block

def convolutional_block(X, f, filters, s = 2, training=True, \
                        initializer=glorot_uniform):
    """
    Implementation of the convolutional block, which Skip connection 
    "skips over" 3 layers and resize the input 𝑥 to a different dimension.
    
    Arguments:
    X -- input tensor of shape (m, n_H_prev, n_W_prev, n_C_prev)
    f -- integer, specifying the shape of the middle CONV's window 
         for the main path
    filters -- python list of integers, defining the number of filters 
               in the CONV layers of the main path
    s -- Integer, specifying the stride to be used
    training -- True: Behave in training mode
                False: Behave in inference mode
    initializer -- to set up the initial weights of a layer. 
                   Equals to Glorot uniform initializer, 
                   also called Xavier uniform initializer.
    
    Returns:
    X -- output of the convolutional block, tensor of shape (n_H, n_W, n_C)
    """
    
    # Retrieve Filters
    F1, F2, F3 = filters
    
    # Save the input value
    X_shortcut = X

    ##### MAIN PATH #####
    
    # First component of main path glorot_uniform(seed=0)
    X = Conv2D(filters = F1, kernel_size = 1, strides = (s, s),padding='valid',\
               kernel_initializer = initializer(seed=0))(X)
    X = BatchNormalization(axis = 3)(X, training=training)
    X = Activation('relu')(X)
    
    # Second component of main path 
    X = Conv2D(filters = F2, kernel_size = (f,f), strides = (1, 1),\
               padding='same',\
               kernel_initializer = initializer(seed=0))(X)
    X = BatchNormalization(axis = 3)(X, training=training) 
    X = Activation('relu')(X) 

    # Third component of main path 
    X = Conv2D(filters = F3, kernel_size = 1, strides = (1, 1), \
               padding='valid',\
               kernel_initializer = initializer(seed=0))(X)
    X = BatchNormalization(axis = 3)(X, training=training) 
    
    ##### SHORTCUT PATH ##### 
    X_shortcut = Conv2D(filters = F3, kernel_size = 1, strides = (s, s),\
                        padding='valid',\
                        kernel_initializer = initializer(seed=0))(X_shortcut)
    X_shortcut = BatchNormalization(axis = 3)(X_shortcut, training=training)

    # Final step: Add shortcut value to main path
    X = Add()([X, X_shortcut])
    X = Activation('relu')(X)
    
    return X


# GRADED FUNCTION: ResNet50

def ResNet50(input_shape = (64, 64, 3), classes = 6):
    """
    Stage-wise implementation of the architecture of the popular ResNet50:
    CONV2D -> BATCHNORM -> RELU -> MAXPOOL -> CONVBLOCK -> IDBLOCK*2 -> 
    CONVBLOCK -> IDBLOCK*3 -> CONVBLOCK -> IDBLOCK*5 -> CONVBLOCK -> 
    IDBLOCK*2 -> AVGPOOL -> FLATTEN -> DENSE 

    Arguments:
    input_shape -- shape of the images of the dataset
    classes -- integer, number of classes

    Returns:
    model -- a Model() instance in Keras
    """
    
    # Define the input as a tensor with shape input_shape
    X_input = Input(input_shape)
    
    # Zero-Padding
    X = ZeroPadding2D((3, 3))(X_input)
    
    # Stage 1
    X = Conv2D(64, (7, 7), strides = (2, 2), \
               kernel_initializer = glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis = 3)(X)
    X = Activation('relu')(X)
    X = MaxPooling2D((3, 3), strides=(2, 2))(X)

    # Stage 2
    X = convolutional_block(X, f = 3, filters = [64, 64, 256], s = 1)
    X = identity_block(X, 3, [64, 64, 256])
    X = identity_block(X, 3, [64, 64, 256])
     
    ## Stage 3 
    X = convolutional_block(X, f = 3, filters = [128, 128, 512], s = 2) 
    X = identity_block(X, 3, [128, 128, 512])
    X = identity_block(X, 3, [128, 128, 512])
    X = identity_block(X, 3, [128, 128, 512])
    
    ## Stage 4 
    X = convolutional_block(X, f = 3, filters = [256, 256, 1024], s = 2)  
    X = identity_block(X, 3, [256, 256, 1024]) 
    X = identity_block(X, 3, [256, 256, 1024])  
    X = identity_block(X, 3, [256, 256, 1024])  
    X = identity_block(X, 3, [256, 256, 1024])  
    X = identity_block(X, 3, [256, 256, 1024])  

    ## Stage 5 
    X = convolutional_block(X, f = 3, filters = [512, 512, 2048], s = 2) 
    X = identity_block(X, 3, [512, 512, 2048]) 
    X = identity_block(X, 3, [512, 512, 2048]) 

    ## AVGPOOL
    X = AveragePooling2D(pool_size=(2, 2))(X)

    # output layer
    X = Flatten()(X)
    X = Dense(classes, activation='softmax', \
              kernel_initializer = glorot_uniform(seed=0))(X)
    
    model = Model(inputs = X_input, outputs = X)

    return model
