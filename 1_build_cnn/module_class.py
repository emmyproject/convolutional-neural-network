#***********************************************************************
#Module class
#All the classes and functions are define dhere
#***********************************************************************
import h5py
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras.layers as tfl
import textwrap
#----------------------------------------------------------------------
#Class part

#Colour Class

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


#Class of loading, impleenting and plotting data

class dataset():

    def __init__(self,train_name,test_name):
        self.train_name=train_name
        self.test_name=test_name

    def load_dataset(self):
        train_dataset = h5py.File('datasets/'+self.train_name, "r")

        # your train set features
        train_set_x_orig = np.array(train_dataset["train_set_x"][:])

        # your train set labels
        train_set_y_orig = np.array(train_dataset["train_set_y"][:]) 

        test_dataset = h5py.File('datasets/'+self.test_name, "r")

        # your test set features
        test_set_x_orig = np.array(test_dataset["test_set_x"][:])

        # your test set labels
        test_set_y_orig = np.array(test_dataset["test_set_y"][:]) 

        # the list of classes
        classes = np.array(test_dataset["list_classes"][:]) 
    
        train_set_y_orig = train_set_y_orig.reshape((1, \
                                                     train_set_y_orig.shape[0]))
        test_set_y_orig = test_set_y_orig.reshape((1, \
                                                   test_set_y_orig.shape[0]))
    
        return train_set_x_orig, train_set_y_orig, test_set_x_orig,\
            test_set_y_orig, classes


    def display_sample(self,dataset):

        print ('Do you like to display a sample training image?')
        ans=str(input('(y/n): \n')).lower().strip()
        try:
            if ans[0] == 'y':
                print('\nPlease enter an image index\n')
                image_index=int(input('between 0 and '\
                                      +str(dataset.shape[0]-1)+'\n'))
                plt.imshow(dataset[image_index])
                plt.show()

            elif ans[0] == 'n':
                pass
            else:
                print ('\nTry again.')
                return self.display_sample(dataset)
        except ValueError:
            print('Please enter valid inputs')
            print('error')
            return self.display_sample()
            


    def norm_reshape_image(self, x_train, y_train, x_test, y_test, \
                           func,print_out = False):
        # Normalize image vectors
        x_train = x_train/255.
        x_test = x_test/255.

        # Reshape
        if func == 'seq':
            y_train = y_train.T
            y_test = y_test.T
        if func == 'functional':
            #6 type of signs there are in the dataset 
            y_train = np.eye(6)[y_train.reshape(-1)]
            y_test = np.eye(6)[y_test.reshape(-1)]

        if print_out == True:
            print ('------------------------------------------------------')
            print (bcolors.BOLD+\
                   'Detail of ', self.train_name,'and',self.test_name,'sets:'\
                   +bcolors.ENDC)
            print ("Number of training examples = " + str(x_train.shape[0]))
            print ("Number of test examples = " + str(x_test.shape[0]))
            print ("X_train shape: " + str(x_train.shape))
            print ("Y_train shape: " + str(y_train.shape))
            print ("X_test shape: " + str(x_test.shape))
            print ("Y_test shape: " + str(y_test.shape))
            print ('------------------------------------------------------')

            self.display_sample(x_train)

        return x_train, y_train, x_test, y_test
        
#----------------------------------------------------------------------------
#Function part


# GRADED FUNCTION: happyModel

def happyModel():
    """
    An example of creating a Sequential Model. 
    Implements the forward propagation for the binary classification model:
    ZEROPAD2D -> CONV2D -> BATCHNORM -> RELU -> MAXPOOL -> FLATTEN -> DENSE
    
    Normally, functions should take these values as function parameters.
    
    Arguments:
    None

    Returns:
    model -- TF Keras model (object containing the information for the
             entire training process) 
    """
    model = tf.keras.Sequential(
        [
            ## ZeroPadding2D with padding 3, input shape of 64 x 64 x 3
            tfl.ZeroPadding2D(padding=(3, 3), data_format=None,\
                              input_shape=(64,64,3)),
        
            ## Conv2D with 32 7x7 filters and stride of 1
            tfl.Conv2D(filters=32, kernel_size=(7,7), strides=(1, 1)),
            
            ## BatchNormalization for axis 3
            tfl.BatchNormalization(axis=3), 
        
            ## ReLU
            tfl.ReLU( ),
        
            ## Max Pooling 2D with default parameters
            tfl.MaxPool2D(),

            ## Flatten layer
            tfl.Flatten(),

            ## Dense layer with 1 unit for output & 'sigmoid' activation
            tfl.Dense(units=1, activation='sigmoid')
        
            # YOUR CODE STARTS HERE
            
            
            # YOUR CODE ENDS HERE
            ]
    )
    
    return model


# GRADED FUNCTION: convolutional_model

def convolutional_model(input_shape):
    """
    Implements the forward propagation for the model:
    CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> DENSE
    
    Arguments:
    input_img -- input dataset, of shape (input_shape)

    Returns:
    model -- TF Keras model (object containing the information for the
    entire training process) 
    """

    input_img = tf.keras.Input(shape=input_shape)
    ## CONV2D: 8 filters 4x4, stride of 1, padding 'SAME'
    Z1 = tf.keras.layers.Conv2D(filters=8, kernel_size=(4,4),\
                                strides=(1, 1), padding='same')(input_img)

    ## RELU
    A1 = tf.keras.layers.ReLU(max_value=None, negative_slope=0, threshold=0)(Z1)

    ## MAXPOOL: window 8x8, stride 8, padding 'SAME'
    P1 = tf.keras.layers.MaxPool2D(pool_size=(8, 8), strides=(8,8), \
                                   padding='same', data_format=None)(A1)

    ## CONV2D: 16 filters 2x2, stride 1, padding 'SAME'
    Z2 = tf.keras.layers.Conv2D(filters=16, kernel_size=(2,2), \
                                strides=(1, 1), padding='same')(P1)
    
    ## RELU
    A2 = tf.keras.layers.ReLU(max_value=None, negative_slope=0,\
                              threshold=0)(Z2)
        
    ## MAXPOOL: window 4x4, stride 4, padding 'SAME'
    P2 = tf.keras.layers.MaxPool2D(pool_size=(4, 4), strides=(4,4), \
                                   padding='same', data_format=None)(A2)
    
    ## FLATTEN
    F = tf.keras.layers.Flatten(data_format=None)(P2)

    
    ## Dense layer
    ## 6 neurons in output layer. Hint: one of the arguments should be
    ##"activation='softmax'" 
    outputs = tf.keras.layers.Dense(units=6, activation='softmax', \
                                    use_bias=True,\
                                    kernel_initializer='glorot_uniform',\
                                    bias_initializer='zeros', \
                                    kernel_regularizer=None, \
                                    bias_regularizer=None, \
                                    activity_regularizer=None,
                                    kernel_constraint=None, \
                                    bias_constraint=None)(F)

    model = tf.keras.Model(inputs=input_img, outputs=outputs)
    return model
