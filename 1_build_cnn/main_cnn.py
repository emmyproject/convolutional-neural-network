#**************************************************************************
#EM 23/09/2021
#Main program about "Convolutional Neural Networks & Application"
#Create a mood classifer using the TF Keras Sequential API
#Build a ConvNet to identify sign language digits using
#the TF Keras Functional API
#**************************************************************************

import numpy as np
import math
import scipy
import textwrap
from module_class import *
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras.layers as tfl
import pandas as pd
#from matplotlib.pyplot import imread
#from PIL import Image
#from tensorflow.python.framework import ops

np.random.seed(1)
#----------------------------------------------------------------------------
print(bcolors.BOLD+'\nBUILDING A CONVOLUTIONAL NEURAL NETWORK')
print('=================================================\n'+bcolors.ENDC)

print (textwrap.fill('NOTE: If you like more details about'
                     'the dataset and display a sample training image, '
                     'set "print_out = True" in function of '
                     'norm_reshape_image.')+'\n')
#Ask user wherther they like to use sequential or functional API.
while True:
                         
    print ('Would you like to:\n ')

    print (textwrap.fill('(1) Build a model using Keras sequential API?'
                         'This allows you to build layer by layer,'
                         'and is ideal for building models where each'
                         'layer has exactly one input tensor and one '
                         'output tensor. It is only appropriate for simpler,'
                         'more straightforward tasks.'
                         'To implement the inputs,'
                         'check class_module, function happyModel')+'\n')

    print(textwrap.fill('(2) Build a ConvNet model by using'
                        'Keras Functional API?'
                        'The output can differentiate between 6 sign digits'
                        'The Functional API can handle models with non-linear'
                        'topology, shared layers, as well as layers with'
                        'multiple inputs or outputs.')+'\n')

    try:
        ichoice=int(input('Please enter 1 or 2: '))
    except ValueError:
        ichoice=-1
    if ichoice>=1 and ichoice<=2:
        break
    print("Try again.  Enter an integer 1 or 2.\n")
print(' ')
        
if ichoice==1:
    #Use the sequential API 
    #LOAD THE DATA AND SPLIT INTO TRAIN/TEST SETS
    #for happy sets
    data=dataset('train_happy.h5','test_happy.h5')

    X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = \
        data.load_dataset()

    #normalise, reshape the images.
    X_train, Y_train, X_test, Y_test = \
        data.norm_reshape_image(X_train_orig, Y_train_orig, X_test_orig,\
                                Y_test_orig, func='seq', print_out = False)

    #The sequential API example defined in module_class where
    #some inputs are provided
    print(bcolors.BOLD+'\nCreating a sequential happyModel'+bcolors.ENDC)
    happy_model = happyModel()
    
    #Compile the model for training with an optimizer and loss of your choice.
    #When the string accuracy is specified as a metric,
    #the type of accuracy used will be automatically converted
    #based on the loss function used
    happy_model.compile(optimizer='adam',
                        loss='binary_crossentropy',
                        metrics=['accuracy'])

    print (bcolors.BOLD+'\nChecking the model parameters\n'+bcolors.ENDC)
    happy_model.summary()

    #TRAIN AND EVALUATE THE MODEL AGAINST THE TEST SET
    happy_model.fit(X_train, Y_train, epochs=10, batch_size=16)

    print (bcolors.BOLD+'\nPrinting the value of the loss function')
    print ('and the performance metrics:'+bcolors.ENDC)
    happy_model.evaluate(X_test, Y_test)

#------------------------------------------------------------------------------
elif ichoice == 2:
    #Build a ConvNet by using Keras Functional API.

    # Loading the data (signs)
    data_sign=dataset('train_signs.h5','test_signs.h5')

    X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = \
        data_sign.load_dataset()

    #normalise, reshape the images.
    X_train, Y_train, X_test, Y_test = \
        data_sign.norm_reshape_image(X_train_orig, Y_train_orig, X_test_orig,\
                                     Y_test_orig, func='functional',\
                                     print_out = False)

    print ('\nBuilding the convolutional model ...\n')
    conv_model = convolutional_model((64, 64, 3))
    conv_model.compile(optimizer='adam',\
                       loss='categorical_crossentropy',
                       metrics=['accuracy'])
    conv_model.summary()

    print ('\nTraining the model ...\n')
    train_dataset = tf.data.Dataset.from_tensor_slices((X_train, Y_train)).\
        batch(64)
    test_dataset = tf.data.Dataset.from_tensor_slices((X_test, Y_test)).\
        batch(64)
    history = conv_model.fit(train_dataset, epochs=100, \
                             validation_data=test_dataset)

    print (textwrap.fill('\nPrinting output of the .fit() operation'
                         'and provides a record of all the loss and'
                         'metric values in memory')+'\n')

    hist=history.history

    #visualize the loss over time using history.history
    #The history.history["loss"] entry is a dictionary
    #with as many values as epochs that the
    # model was trained on. 
    df_loss_acc = pd.DataFrame(hist)
    df_loss= df_loss_acc[['loss','val_loss']]
    df_loss.rename(columns={'loss':'train','val_loss':'validation'},\
                   inplace=True)
    df_acc= df_loss_acc[['accuracy','val_accuracy']]
    df_acc.rename(columns={'accuracy':'train','val_accuracy':'validation'},\
                  inplace=True)
    df_loss.plot(title='Model loss',figsize=(12,8)\
    ).set(xlabel='Epoch',ylabel='Loss')
    df_acc.plot(title='Model Accuracy',figsize=(12,8)\
    ).set(xlabel='Epoch',ylabel='Accuracy')
    plt.show()

    

    


